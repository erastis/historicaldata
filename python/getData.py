import cbpro
import os
from datetime import datetime, date, timedelta
import pandas as pd
from colorama import Back, Fore, Style
import time

public_client = cbpro.PublicClient()


def datesInput():
    # Input start and end dates

    while True:
        try:
            today = str(datetime.today().date())
            format = '%Y-%m-%d'
            startDate = input(
                f'{Fore.LIGHTCYAN_EX}\nEnter start date as follows YYYY-MM-DD: {Style.RESET_ALL}')
            endDate = input(
                f'{Fore.LIGHTCYAN_EX}Enter end date as follows YYYY-MM-DD: {Style.RESET_ALL}')
            datetime.strptime(startDate, format)
            datetime.strptime(endDate, format)

        except ValueError:
            print(f'{Fore.LIGHTRED_EX}Incorrect Format{Style.RESET_ALL}')
            continue

        if startDate >= endDate:
            print(
                f'{Fore.LIGHTRED_EX}Starting date must be prior to end date{Style.RESET_ALL}')
            continue

        if today < endDate:
            print(
                f'{Fore.LIGHTRED_EX}Ending date must be before {today}{Style.RESET_ALL}')
            continue

        if ((pd.to_datetime(endDate) - pd.to_datetime(startDate)).days > 300):
            print(
                f'{Fore.LIGHTRED_EX}There must be max. 300 days between starting and ending date {today}{Style.RESET_ALL}')
            continue

        else:
            # input is correct -> exit loop
            break

    return(startDate, endDate)


def coinInput():
    # Input Coin Symbol

    csvData = os.path.join('./data', 'coinList.csv')
    dfCoin = pd.read_csv(csvData)
    coinList = []

    for i in dfCoin['Symbol']:
        coinList.append(i)

    while True:
        coin = input(
            f'{Fore.LIGHTCYAN_EX}\nEnter Coin Symbol i.e. ETH-USD: {Style.RESET_ALL}')

        if (coin not in coinList):
            print(f'{Fore.LIGHTRED_EX}Unknown symbol, try again...{Style.RESET_ALL}')
            continue

        else:
            # input is correct -> exit loop
            break

    return(coin)


# Get historical Data
def getData(start, end, coin):
    # Retrieve Data: timestamp Low High Open Close Volume
    rawData = public_client.get_product_historic_rates(
        coin, start, end, granularity=86400)

    # remove Volume
    rawData = [item[0:5] for item in rawData]

    # columns names
    columns = ['timestamp', 'low', 'high', 'open', 'close']

    # new dataframe
    df = pd.DataFrame(rawData, columns=columns)

    # convert timestamp to date
    dateList = []
    for i in df.index:
        date = datetime.fromtimestamp(df['timestamp'][i]).date()
        dateList.append(date)

    # add mean column
    meanList = []
    for i in df.index:
        meanPrice = (df['high'][i] + df['low'][i])/2
        meanList.append(round(meanPrice, 2))

    # replace timestamp by date
    del df['timestamp']
    df['date'] = dateList
    df['mean'] = meanList

    # reoder columns
    df = df[['date', 'mean', 'low', 'high', 'open', 'close']]

    if (df.isnull().sum().sum() > 0):
        f'{Fore.LIGHTRED_EX}Be careful, dataframe contains NaN values{Style.RESET_ALL}'

    df.to_csv(r'./' + str(coin) + '.csv', index=False)


if __name__ == '__main__':
    try:
        datesOutput = datesInput()
        start = datesOutput[0]
        end = datesOutput[1]
        coin = coinInput()

        # Calculate execution time
        start_time = time.perf_counter()
        getData(start, end, coin)
        end_time = time.perf_counter()
        print(f'\nExecution time: {round((end_time - start_time), 4)}s')
    except ValueError:
        print('Error, try to change parameters')
