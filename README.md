# Retrieve Historical Data

###### Tutorial
> 1) Download the folder
> 2) Go to the Python folder
> 3) Make sure sure that all required packages have been installed in your python environment
> 4) Run the getData.py file 
> 5) Specify starting date, ending date and coin pair
> 6) Retrieve the generated csv file in the folder

> ⚠️ Max 300 days between starting and ending dates 

###### List of Currency Pairs: 
> [Coins](https://gitlab.com/erastis/historicaldata/-/tree/master/python/data/coinList.csv)

###### Output example:

> [Output](https://gitlab.com/erastis/historicaldata/-/tree/master/python/data/ETH-USD.csv)
